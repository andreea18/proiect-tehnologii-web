# Clasificare și număr de înregistrare pentru documente electronice
# -integrare cu Google Drive-


### Backend: NodeJS, Express.JS, Sequelize.JS, MySQL
### Frontend: ReactJS

# Funcționalități: 
### - citire fișiere de pe Drive
### - download fișiere
### - upload fișiere
### - ștergere fișiere

### Pe partea de frontend voi folosi componente pentru fiecare concept.

### La backend, pentru baze de date, mă voi ajuta de Sequelize.JS pentru a face legătura dintre baza de date și aplicația scrisă în NodeJS, iar baza de date va fi de tip MySQL.
