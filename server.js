const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const cors = require("cors");

const application = express();

let connection = new Sequelize('e_documents', 'root', '', { dialect: 'mysql' });

let User = connection.define('users', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    nr_documents: {
        type: Sequelize.INTEGER
    }
});

let DocumentType = connection.define('documents_types', {
    name: {
        type: Sequelize.STRING
    }
});

let Document = connection.define('documents', {
    name: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.INTEGER,
        references: {
            model: 'documents_types',
            key: 'id'
        }
    }
});

let DocumentsUsers = connection.define('documents_users', {
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: 'users',
            key: 'id'
        }
    },
    document_id: {
        type: Sequelize.INTEGER,
        references: {
            model: 'documents',
            key: 'id'
        }
    }
});

connection.sync();
// connection.sync({ force: true });

application.use(express.static('frontend/e_documents/public/'));
application.use(bodyParser.json({limit: '10mb'}));
application.use(express.bodyParser({limit: '10mb'}));
application.use(bodyParser.urlencoded({limit: "10mb", extended: true, parameterLimit:10000}));
application.use(cors());

application.get("/documents", (request, response) => {
    Document.findAll({attributes: ['id', 'name', 'type']})
    .then(documents => response.status(201).send(documents))
    .catch(error => response.status(500).send({ code: 0, body: "There was a problem getting the posts" }));
});

// users

application.get("/users", (request, response) => {
    User.findAll()
        .then(users => {
            response.status(201).send(users);
        })
        .catch(error => response.status(500).send("There was a problem getting the users"));
});

application.post("/users/add", (request, response) => {
    let user = request.body;
    
    User.create(user)
        .then(() => response.status(201).send("The user has been created succesfully"))
        .catch((error) => {
            console.warn(error);
            response.send("There are errors that need to be revised");
    });
});

// documents types

application.get("/documents/types", (request, response) => {
    DocumentType.findAll()
        .then(documentsTypes => {
            response.status(201).send(documentsTypes);
        })
        .catch(error => response.status(500).send("There was a problem getting the documents types"));
});

application.post("/documents/types/add", (request, response) => {
    let documentType = request.body;
    
    DocumentType.create(documentType)
        .then(() => response.status(201).send("The document type has been created succesfully"))
        .catch((error) => {
            console.warn(error);
            response.send("There are errors that need to be revised");
    });
});

application.get("/types", (request, response) => {
    DocumentType.findAll({attributes: ['id', 'name']})
    .then(types => response.status(201).send(types))
    .catch(error => response.status(500).send({ code: 0, body: "There was a problem getting the documents types" }));
});

application.delete("/delete-document-type/:id", (request, response) => {
    DocumentType.destroy({
        where: { id: request.params.id }
    })
    .then(documentType => response.status(201).send({ status: 1, code: 513, body: "The document type has been succesfully deleted" } ))
    .catch(error => response.status(500).send({ status: 0, code: 503, body: "There are errors that need to be revised" } ));
});


application.listen(8081);